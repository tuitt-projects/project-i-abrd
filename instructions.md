
# 230703 - Batch XXX:  Academic Reading List
## 

This repository contains an academic reading list with various topics and sessions. The purpose of this list is to provide bootcampers with a curated collection of academic resources. Each topic/session has a defined purpose and goals for checking.

## How to Use

1. Clone the repository to your local machine:
```bash
git clone https://gitlab.com/tuitt-projects/project-i-abrd/-/blob/master/batch-XXX-ARD.md 
```

2. Navigate to the repository's directory:
```bash
cd batch-XXX-ARD
```

3. Open the `ard-template.md` file in your preferred text editor to view the list.

4. Add the list of topics from the current/upcoming sessions of the batch.

5. Each topic/session has two columns: "Purpose" and "Goal to Checking".

    - **Purpose**: Describes the main objective or intention of the topic/session. It provides an overview of what bootcampers can expect to learn or gain from studying the resources within that topic/session.

    - **Goal to Checking**: Specifies the specific outcomes or achievements you should aim for when reviewing the resources related to the topic/session. 



