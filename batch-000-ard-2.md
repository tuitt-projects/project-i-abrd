# 230703 - Batch 000:  Academic Reading List 


| Topics / Session       | Purpose           | Goal to Checking  |
| ------------------- | ----------------- | ----------------- |
| ES6 Updates<br>- https://www.w3schools.com/js/js_es6.asp<br>-https://exploringjs.com/es6/ch_overviews.html <br> Selection Control<br>- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/if...else             | Create Javascript functions using the arrow function syntax, perform array and object destructuring | Create a student grading system using an arrow function. The grade categories are as follows: Failed(74 and below), Beginner (75-80), Developing (81-85), Above Average (86-90), Advanced(91-100) |
| For Loop Statements <br> - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for | Create a Javascript For statements | Create an odd-even checker that will check which numbers from 1-300 are odd and which are even. <br> <br> Sample output in the console: <br> 1 - odd <br> 2 - even <br> 3 - odd <br> 4 - even <br> 5 - odd <br> etc. |
| NodeJS <br> - https://www.w3schools.com/nodejs/nodejs_intro.asp <br> NodeJS HTTP Module <br> - https://www.w3schools.com/nodejs/nodejs_http.asp           | Create a simple server using the HTTP module. | **Instructions:** <br> a. Create an `index.js` file inside the `readingListActC` folder. <br> b. Import the `http` module using the `require` directive. Create a variable `port` and assign it the value `8000`. <br> c. Create a server using the `createServer` method that will listen to the port provided above. <br> d. Console log a message in the terminal when the server is successfully running. <br> e. Create a condition that prints a specific message when the following routes are accessed: login, register, homepage, and usersProfile. <br> f. Test each route to ensure they are working as intended. Save a screenshot for each test. <br> g. Create a condition for any other routes that will return an error message. |
| Topic 4             | Purpose of Topic 4 | Goals for Topic 4 |
| Topic 5             | Purpose of Topic 5 | Goals for Topic 5 |
| Topic 6             | Purpose of Topic 6 | Goals for Topic 6 |






